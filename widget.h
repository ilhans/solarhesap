#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QFile>
#include <QFileDialog>
#include <QDebug>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void writeResults();

private slots:
    void on_importSolarData_clicked();

    void on_importLoadData_clicked();

    void on_calculateResults_clicked();

    void on_batteryCapacity_textChanged(const QString &arg1);

private:
    Ui::Widget *ui;
    double batteryCapacity = 0.0;
    double solarData[365][24] = {0};
    double loadData[365][24] = {0};
    double toGrid[365][24] = {0};
    double fromGrid[365][24] = {0};
    double batterySOC[365][24] = {0};
    bool checkSolar = false, checkLoad = false, checkBattery = false;
    void showAlert(QString message, int width, int height);
};

#endif // WIDGET_H
