#include "widget.h"
#include "ui_widget.h"
#include <QMessageBox>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_importSolarData_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Import Solar Data From File"), QDir::currentPath(), tr("Data Files (*.txt *.xlsx *.docx)"));
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            qDebug() << "File cannot be opened";
    } else {
            ui->importSolarCheck->setText("<p style='color: #01DF3A; font-size: 16px;'>&#10004;</p>");
            checkSolar = true;
    }

    int hours = 0;
    int days = 0;
    QTextStream in(&file);
    while (!in.atEnd()) {

        bool ok;
        QLocale loc(QLocale::German);
        QString line = in.readLine();
        double x = loc.toDouble(line, &ok);

        if (!ok)
            qDebug() << "Conversion Failed";
        else
            solarData[days][hours] = x;
        hours++;
        if(hours == 24) {

            hours = 0;
            days++;
        }
    }
    file.close();
}

void Widget::on_importLoadData_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Import Load Data From File"), QDir::currentPath(), tr("Data Files (*.txt *.xlsx *.docx)"));
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            qDebug() << "File cannot be opened";
    } else {
        ui->importLoadCheck->setText("<p style='color: #01DF3A; font-size: 16px;'>&#10004;</p>");
        checkLoad = true;
    }

    int hours = 0;
    int days = 0;
    QTextStream in(&file);
    while (!in.atEnd()) {

        bool ok;
        QLocale loc(QLocale::German);
        QString line = in.readLine();
        double x = loc.toDouble(line, &ok);

        if (!ok)
            qDebug() << "Conversion Failed";
        else
            loadData[days][hours] = x;
        hours++;
        if(hours == 24) {

            hours = 0;
            days++;
        }
    }
    file.close();
}

void Widget::on_calculateResults_clicked()
{
    if (checkBattery && checkLoad && checkSolar) {
        double initialsOC = batteryCapacity*0.2, sOC = batteryCapacity * 0.2;

        // A day starts at 22:00. So [0][0] means Dec 31 22:00
        // @todo: Solar data should be utilized more!
        for(int i=0; i<365; i++) {
            double solarProduction = 0.0;
            double daytimeConsumption = 0.0;
            for(int j=0; j<24; j++) {
                solarProduction += solarData[i][j];
            }
            // Think this through!
            for(int k=8; k<19; k++) {
                daytimeConsumption += loadData[i][k];
            }

            if (solarProduction <= daytimeConsumption) {
                for(int a=0; a<8; a++) {
                    fromGrid[i][a] = loadData[i][a] + (batteryCapacity - initialsOC) / 8.0;
                    sOC += (batteryCapacity - initialsOC) / 8.0;
                    batterySOC[i][a] = sOC;
                }
                for(int b=8; b<19; b++) {
                    batterySOC[i][b] = sOC;
                    if(solarData[i][b] >= loadData[i][b]) {
                        toGrid[i][b] = solarData[i][b] - loadData[i][b];
                    } else {
                        fromGrid[i][b] = loadData[i][b] - solarData[i][b];
                    }
                }
                for(int c=19; c<24; c++) {
                    if((sOC-loadData[i][c]) >= batteryCapacity*0.2) {
                        sOC -= loadData[i][c];
                        batterySOC[i][c] = sOC;
                    } else if(sOC > batteryCapacity*0.2) {
                        fromGrid[i][c] = loadData[i][c] - (sOC - batteryCapacity*0.2);
                        sOC = batteryCapacity*0.2;
                        batterySOC[i][c] = sOC;
                    } else {
                        fromGrid[i][c] = loadData[i][c];
                        batterySOC[i][c] = sOC;
                    }
                }
                initialsOC = sOC;
            } else {
                double batteryWillBeChargedUpto;
                if((solarProduction-daytimeConsumption)>= (batteryCapacity-initialsOC))
                    batteryWillBeChargedUpto = initialsOC;
                else
                    batteryWillBeChargedUpto = batteryCapacity - (solarProduction - daytimeConsumption);

                for(int a=0; a<8; a++) {
                    if(batteryWillBeChargedUpto == initialsOC) {
                        fromGrid[i][a] = loadData[i][a];
                        batterySOC[i][a] = sOC;
                    } else {
                        fromGrid[i][a] = loadData[i][a] + (batteryWillBeChargedUpto - initialsOC) / 8.0;
                        sOC += (batteryWillBeChargedUpto - initialsOC) / 8.0;
                        batterySOC[i][a] = sOC;
                    }
                }
                for(int b=8; b<19; b++) {
                    if(solarData[i][b] >= loadData[i][b]) {
                        if(sOC + (solarData[i][b] - loadData[i][b]) <= batteryCapacity) {
                            sOC += solarData[i][b] - loadData[i][b];
                            batterySOC[i][b] = sOC;
                        } else if(sOC < batteryCapacity) {
                            toGrid[i][b] = solarData[i][b] - loadData[i][b] - (batteryCapacity-sOC);
                            sOC = batteryCapacity;
                            batterySOC[i][b] = sOC;
                        } else {
                            toGrid[i][b] = solarData[i][b] - loadData[i][b];
                            batterySOC[i][b] = sOC;
                        }
                    } else {
                        fromGrid[i][b] = loadData[i][b] - solarData[i][b];
                        batterySOC[i][b] = sOC;
                    }
                }

                for(int c=19; c<24; c++) {
                    if((sOC-loadData[i][c]) >= batteryCapacity*0.2) {
                        sOC -= loadData[i][c];
                        batterySOC[i][c] = sOC;
                    } else if(sOC > batteryCapacity*0.2) {
                        fromGrid[i][c] = loadData[i][c] - (sOC - batteryCapacity*0.2);
                        sOC = batteryCapacity*0.2;
                        batterySOC[i][c] = sOC;
                    } else {
                        fromGrid[i][c] = loadData[i][c];
                        batterySOC[i][c] = sOC;
                    }
                }
                initialsOC = sOC;
            }
        }
        writeResults();
        double price = 0.0, priceThreeTime = 0.0, priceNormal = 0.0;
        for(int m=0; m<365; m++) {
            for(int k=0; k<8; k++) {
                price += (fromGrid[m][k]*(9.7299+11.0813) + fromGrid[m][k]*9.7299*0.08)*1.18;
                priceThreeTime += (loadData[m][k]*(9.7299+11.0813) + loadData[m][k]*9.7299*0.08)*1.18;
                priceNormal += (loadData[m][k]*(22.1020 + 11.0813) + loadData[m][k]*22.1020*0.08)*1.18;
            }
            for(int l=8; l<19; l++) {
                price += (fromGrid[m][l]*(21.9634+11.0813) + fromGrid[m][l]*21.9634*0.08)*1.18;
                priceThreeTime += (loadData[m][l]*(21.9634+11.0813) + loadData[m][l]*21.9634*0.08)*1.18;
                priceNormal += (loadData[m][l]*(22.1020 + 11.0813) + loadData[m][l]*22.1020*0.08)*1.18;
            }
            for(int a=19; a<24; a++) {
                price += (fromGrid[m][a]*(38.8971+11.0813) + fromGrid[m][a]*38.8971*0.08)*1.18;
                priceThreeTime += (loadData[m][a]*(38.8971+11.0813) + loadData[m][a]*38.8971*0.08)*1.18;
                priceNormal += (loadData[m][a]*(22.1020 + 11.0813) + loadData[m][a]*22.1020*0.08)*1.18;
            }
        }
        priceThreeTime /= 100;
        priceNormal /= 100;
        price /= 100;
        qDebug() << QString::number(priceNormal, 'f', 2) << " " << QString::number(priceThreeTime, 'f', 2);
        ui->priceOutput->setText(QString::number(price, 'f', 2) + " TL");

    } else {
        showAlert("Solar and load data should be imported. Battery capacity should be entered!", 500, 200);
    }
}

// Write to .csv file
void Widget::writeResults() {

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Results"), QDir::currentPath()+"/test.csv", tr("Data Files (*.csv)"));
    QFile file(fileName);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        qDebug() << "File cannot be opened for writing!";
        return;
    }

    QTextStream out(&file);
    out << "day" << ";" << "hour" << ";" << "solarProd" << ";" << "Load" << ";" << "batteryState" << ";" << "fromGrid" << ";" << "toGrid" << "\n";
    for(int i=0; i<365; i++) {
        for(int j=0; j<24; j++) {
            QLocale loc(QLocale::German);
            QString fromG = loc.toString(fromGrid[i][j], 'f', 6);
            QString toG = loc.toString(toGrid[i][j], 'f', 6);
            QString solarProd = loc.toString(solarData[i][j], 'f', 9);
            QString load = loc.toString(loadData[i][j], 'f', 6);
            QString batteryState = loc.toString(batterySOC[i][j], 'f', 6);

            out << QString::number(i+1) << ";" << QString::number((j+22)%24) << ";" << solarProd << ";" << load << ";" << batteryState << ";" << fromG << ";" << toG << "\n";
        }
    }

    file.flush();
    file.close();
}
void Widget::on_batteryCapacity_textChanged(const QString &arg1)
{
    bool ok;
    if(!arg1.isEmpty()) {
        QLocale loc(QLocale::German);
        batteryCapacity = loc.toDouble(&arg1, &ok);

        if(!ok) {
            showAlert("Please enter a valid battery capacity in the form of xx,yy in kWh", 500,200);
            ui->batteryCheck->setText("*");
        } else {
            checkBattery = true;
            ui->batteryCheck->setText("<p style='color: #01DF3A; font-size: 16px;'>&#10004;</p>");
        }
    }
    else {
        ui->batteryCheck->setText("*");
    }
}

void Widget::showAlert(QString message, int width, int height)
{
    QMessageBox messageBox;
    messageBox.critical(this, "Error", message);
    messageBox.setFixedSize(width, height);
}

