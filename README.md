# README #

SOLAR BATTERY CALCULATION

### What is this repository for? ###

* Code is to take hourly solar generation and load data for one year and calculate the reduction in annual expenditure for electricity for a given battery capacity. An algorithm about when to use the battery power or grid power is included.
* Version: 1.0
* Still in progress